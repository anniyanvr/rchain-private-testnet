# RChain Private Testnet

This repo sets ap a Docker environment that runs several RChain validators.

## Requirements
You must have Docker installed. *Testest on `Docker version 18.06.1-ce, build e68fc7a`*

## Getting started
Prior to launching the containers you must first populate the `datadir`.  The `init_datadir` directory contains a copy of what `datadir` should have in its initial state.

*Note: To restart the nodes in their initial state, just re-populate `datadir`.*

Pupolate the `datadir` with the initial state.
```
cp -R init_datadir/* datadir
```

Launch the network with docker-compose
```
docker-compose up -d
```

## RCHain private testnet environment

This docker-compose.yml will launch two validators in a docker environment with bridge network. One is called `rnode-bootstrap` the other is called `rnode1`. If you want to add a third node, create an rnode2 directory off `datadir` that is setup limialr to `rnode1`. You can also give it one of the private keys found in the `genesis` directory.

It also maps ports 40400-40404 from your host to the the node called `rnode-bootstrap`.

There is also a container called `node-sh` based on `node:carbon` that is launched into thise environment. It sits there idely running `sleep 100000`, waiting for someone to connect to launch commands against the validators in the environment.  To connect to it just doe the following:

```
docker exec -it node-sh /bin/bash
```

*Note: If there are old containers that have stopped but not been removed, docker-compose will modify the names of the running containers.  Use `doecker ps` to figure out the name it assigned.*

### Container name

By default, this will lauch `rchain/rnode:latest`. You can specify the name of the docker container you want to run by setting the environment vairable `RC_IMG`.
```
RC_IMG=mylocalcontainer docker-compose up
```


